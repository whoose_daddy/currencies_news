package com.currencies.news;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.currencies.news.di.AppComponent;
import com.currencies.news.di.AppModule;
import com.currencies.news.di.DaggerAppComponent;
import com.currencies.news.utils.ReleaseTimberTree;

import net.danlew.android.joda.JodaTimeAndroid;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class App extends Application {

    /**
     * Application instance
     */
    private volatile static App instance;

    /**
     * Main application {@link dagger.Component}
     */
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        // Tools init
        JodaTimeAndroid.init(this);
        initCrashlytics();
        initAppComponent();
        initTimber();
    }

    ///////////////////////////////////////////////////////////////////////////
    // TOOLS INIT
    ///////////////////////////////////////////////////////////////////////////

    private void initCrashlytics() {
        CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build());
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new ReleaseTimberTree(false));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // DEPENDENCY INJECTIONS
    ///////////////////////////////////////////////////////////////////////////

    protected void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getApp() {
        if (instance.appComponent == null) {
            instance.initAppComponent();
        }
        return instance.appComponent;
    }

    public static void clearApp() {
        instance.appComponent = null;
    }

}