package com.currencies.news.widgets;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.currencies.news.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Roman Shylo on 15.04.2017.
 * whoose.daddy@gmail.com
 */

public class AppToolbar extends FrameLayout {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_tvTitle) TextView tvTitle;

    public AppToolbar(@NonNull Context context) {
        super(context);
        init();
    }

    public AppToolbar(
            @NonNull Context context,
            @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppToolbar(
            @NonNull Context context,
            @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_app_toolbar, this);
        ButterKnife.bind(this);
    }

    public void setupWithActivity(AppCompatActivity activity) {
        setupWithActivity(activity, false);
    }

    public void setupWithActivity(AppCompatActivity activity, boolean homeAsUpEnable) {
        activity.setSupportActionBar(toolbar);
        assert activity.getSupportActionBar() != null;
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (homeAsUpEnable) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setTitle(@StringRes int title) {
        setTitle(getContext().getString(title));
    }

    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }

}
