package com.currencies.news.api.entities;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
public class BaseError {

    @SerializedName("description")
    private String error;

    public String getMessage() {
        return error;
    }

}