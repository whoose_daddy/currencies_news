package com.currencies.news.api;

import com.currencies.news.BuildConfig;
import com.currencies.news.api.inerceptors.AuthInterceptor;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by Roman on 14.03.2016
 */
public class RetrofitService {

    private IRetrofitApi retrofitApi;
    private Gson myGson;

    /**
     * constructor
     */
    public RetrofitService(Gson gson) {
        this.myGson = gson;
        createRetrofit();
    }

    public IRetrofitApi getRetrofitApi() {
        return retrofitApi;
    }

    private void createRetrofit() {
        // Enable logging
        HttpLoggingInterceptor interceptorLogging =
                new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Timber.tag("OkHttp.Logger").d(message);
                    }
                });
        interceptorLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Attach auth header to all requests
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new AuthInterceptor())
                .addInterceptor(interceptorLogging)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(client)
                .addConverterFactory(buildGsonConverter())
                .build();
        retrofitApi = retrofit.create(IRetrofitApi.class);
    }

    private GsonConverterFactory buildGsonConverter() {
        return GsonConverterFactory.create(myGson);
    }

}
