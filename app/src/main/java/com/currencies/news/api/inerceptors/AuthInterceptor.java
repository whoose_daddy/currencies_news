package com.currencies.news.api.inerceptors;

import com.currencies.news.app.Const;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Roman Shylo on 04.01.2017.
 * whoose.daddy@gmail.com
 */

public class AuthInterceptor implements Interceptor {

    private static final String QUERY = "app_id";

    /**
     * Add auth token to requests as query parameter
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        HttpUrl.Builder urlBuilder = chain.request().url().newBuilder();

        urlBuilder.addQueryParameter(QUERY, Const.CURRENCIES_API_KEY);

        Request.Builder requestBuilder = chain.request().newBuilder().url(urlBuilder.build());

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
