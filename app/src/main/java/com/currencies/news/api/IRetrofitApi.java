package com.currencies.news.api;

import com.currencies.news.api.entities.Currency;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Spudi on 01.06.2016.
 * whoose.daddy@gmail.com
 * <p>
 * API docs <a href="https://docs.openexchangerates.org/docs/api-introduction">Link</a>
 */
public interface IRetrofitApi {

    @GET("latest.json") Call<Currency> getLatest();

    @GET("historical/{date}.json") Call<Currency> getByDate(@Path("date") String date);

}