package com.currencies.news.api.entities;

import com.currencies.news.app.Const;

import java.util.HashMap;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class Currency {

    private String base;
    private HashMap<String, Float> rates;

    public String getBase() {
        return base;
    }

    public HashMap<String, Float> getRates() {
        return rates;
    }

    public float getBaseRate() {
        if (rates != null && rates.containsKey(Const.CURRENCY_BASE_KEY)) {
            return rates.get(Const.CURRENCY_BASE_KEY);
        }
        return 0;
    }

    @Override public String toString() {
        return Const.CURRENCY_BASE_KEY + " rate: " + getBaseRate();
    }
}
