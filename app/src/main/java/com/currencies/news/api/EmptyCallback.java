package com.currencies.news.api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Roman Shylo on 24.05.2017.
 * whoose.daddy@gmail.com
 */

public class EmptyCallback<T> implements Callback<T> {

    @Override public void onResponse(Call<T> call, Response<T> response) {
        Timber.d("onResponse");
    }

    @Override public void onFailure(Call<T> call, Throwable t) {
        Timber.e(t, "onFailure");
    }
}
