package com.currencies.news.api.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.currencies.news.api.IRetrofitApi;
import com.currencies.news.api.entities.Currency;
import com.currencies.news.utils.AppTimeUtils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class GetCurrenciesByDate extends AsyncTask<Pair<DateTime, DateTime>, Void, List<GetCurrenciesByDate.CurrencyItem>> {

    private IRetrofitApi api;
    private IGetCurrenciesCallback callback;

    public GetCurrenciesByDate(
            @NonNull IRetrofitApi api, @NonNull IGetCurrenciesCallback callback) {
        this.api = api;
        this.callback = callback;
    }

    @Override protected final List<CurrencyItem> doInBackground(Pair<DateTime, DateTime>... pairs) {
        DateTime from = pairs[0].first;
        DateTime to = pairs[0].second;

        int days = Days.daysBetween(from, to).getDays();
        List<CurrencyItem> results = new ArrayList<>(days);
        for (int i = 0; i < days; i++) {
            DateTime date = from.plusDays(i);
            String dateString = AppTimeUtils.formatDate(date);
            results.add(new CurrencyItem(date, getByDate(dateString)));
        }
        return results;
    }

    @Override protected void onPostExecute(List<CurrencyItem> currencies) {
        super.onPostExecute(currencies);
        callback.onCurrenciesReceived(currencies);
    }

    private Currency getByDate(String date) {
        try {
            Response<Currency> response = api.getByDate(date).execute();
            return response.body();
        } catch (IOException e) {
            Timber.e(e, "getByDate");
        }
        return null;
    }

    ///////////////////////////////////////////////////////////////////////////
    // INNER
    ///////////////////////////////////////////////////////////////////////////

    public interface IGetCurrenciesCallback {
        void onCurrenciesReceived(List<CurrencyItem> currencies);
    }

    public class CurrencyItem {

        private DateTime dateTime;
        private Currency currency;

        public CurrencyItem(DateTime dateTime, Currency currency) {
            this.dateTime = dateTime;
            this.currency = currency;
        }

        public DateTime getDateTime() {
            return dateTime;
        }

        public Currency getCurrency() {
            return currency;
        }
    }

}
