package com.currencies.news.api;

import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.currencies.news.App;
import com.currencies.news.R;
import com.currencies.news.api.entities.BaseError;
import com.currencies.news.base_arch.presentation.IBaseView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.EOFException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
public abstract class DefaultCallback<T> implements Callback<T> {

    private WeakReference<IBaseView> mViewRef;

    public DefaultCallback(IBaseView mViewRef) {
        this.mViewRef = new WeakReference<>(mViewRef);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            onResponse(response);
        } else {
            hideLoading();
            handleServerError(response);
        }
    }

    public abstract void onResponse(Response<T> response);

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        hideLoading();
        //output in dialog
        String message;
        if (t instanceof EOFException) {
            message = getString(R.string.errorServer_noInternet);
        } else if (t instanceof IOException) {
            message = getString(R.string.errorServer_noInternet);
        } else {
            message = t.getMessage();
        }
        showDialog(message);
        //log output
        try {
            Timber.e(t, "onFailure: %s", call.request().url().toString());
        } catch (Exception e) {
            Timber.e(t, "onFailure: ");
        }
    }

    private void hideLoading() {
        if (mViewRef != null && mViewRef.get() != null) {
            mViewRef.get().hideProgress();
        }
    }

    protected void handleServerError(Response<T> response) {
        switch (response.code()) {
            case HttpURLConnection.HTTP_NOT_FOUND:
            case HttpURLConnection.HTTP_UNAVAILABLE:
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                showDialog(getString(R.string.errorServer_unavailable));
                break;
            default: {
                String errorMessage = generateMeaningfulMessage(response);
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = getString(R.string.errorServer_default);
                }
                showDialog(errorMessage);
            }
        }
    }

    private void showDialog(String message) {
        Timber.d("showDialog -> message[%s]", message);
        if (mViewRef == null || mViewRef.get() == null) {
            Timber.i("showDialog: mViewRef == NULL");
            return;
        }
        if (TextUtils.isEmpty(message)) {
            Timber.i("showDialog: message is EMPTY");
            return;
        }
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mViewRef.get().ctx());
            builder.setTitle(R.string.app_name);
            builder.setMessage(message);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String generateMeaningfulMessage(Response<T> response) {
        String rawError = getErrorBodyAsRaw(response);
        if (TextUtils.isEmpty(rawError)) {
            return null;
        }
        // Single
        BaseError error = castBodyToSingleError(rawError);
        if (error != null) {
            return error.getMessage();
        }
        // List
        List<BaseError> errors = castBodyToErrorsList(rawError);
        if (errors != null && !errors.isEmpty()) {
            String text = "";
            for (BaseError e : errors) {
                if (text.length() > 0) text += "\n";
                text += e.getMessage();
            }
            return text;
        }
        return rawError;
    }

    private BaseError castBodyToSingleError(String raw) {
        try {
            return new Gson().fromJson(raw, BaseError.class);
        } catch (Exception e) {
            // Ignore
        }
        return null;
    }

    private List<BaseError> castBodyToErrorsList(String raw) {
        try {
            Type type = new TypeToken<List<BaseError>>() {
            }.getType();
            return new Gson().fromJson(raw, type);
        } catch (Exception e) {
            // Ignore
        }
        return null;
    }

    private String getErrorBodyAsRaw(Response<T> response) {
        try {
            return response.errorBody() != null ? response.errorBody().string() : "";
        } catch (IOException e) {
            // Ignore
        }
        return null;
    }

    private String getString(@StringRes int id) {
        return App.getApp().ctx().getString(id);
    }

}