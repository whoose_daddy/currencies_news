package com.currencies.news.utils;

import android.util.Log;

import com.crashlytics.android.core.CrashlyticsCore;
import com.currencies.news.App;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import timber.log.Timber;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class ReleaseTimberTree extends Timber.Tree {

    private boolean writeLogsToFile = false;

    private File logFile;
    private BufferedWriter writer;

    public ReleaseTimberTree(boolean writeLogsToFile) {
        this.writeLogsToFile = writeLogsToFile;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return;
        }
        CrashlyticsCore.getInstance().log(priority, tag, message);
        if (t != null) {
            CrashlyticsCore.getInstance().logException(t);
        }
        // Log to file
        if (writeLogsToFile) {
            writeToFile(tag, message, t);
        }
    }

    private void writeToFile(String tag, String message, Throwable t) {
        if (logFile == null) {
            File root = App.getApp().ctx().getExternalCacheDir();
            logFile = new File(root,
                    "logFile.txt");
        }
        // Write
        try {
            writer = new BufferedWriter(new FileWriter(logFile, true));
            String logString = "Tag: " + tag + " message: " + message;
            if (t != null) {
                logString += " Exception: " + t.getMessage();
            }
            writer.write(logString);
            writer.newLine();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
