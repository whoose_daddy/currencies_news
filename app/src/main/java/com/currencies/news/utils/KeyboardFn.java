package com.currencies.news.utils;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Roman Shylo on 16.05.2017.
 * whoose.daddy@gmail.com
 */

public class KeyboardFn {

    public static void closeKeyboard(Fragment fragment) {
        if (fragment.getView() == null) {
            return;
        }
        View view = fragment.getView().findFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragment.getActivity()
                    .getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void closeKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    0);
        }
    }
}
