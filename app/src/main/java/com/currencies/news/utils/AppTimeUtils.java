package com.currencies.news.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class AppTimeUtils {

    public static String formatDate(DateTime date) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
        return format.print(date);
    }

    public static String formatDateShort(DateTime date) {
        DateTimeFormatter format = DateTimeFormat.forPattern("dd-MM");
        return format.print(date);
    }
}
