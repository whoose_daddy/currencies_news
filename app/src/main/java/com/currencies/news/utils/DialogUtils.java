package com.currencies.news.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import com.currencies.news.R;


/**
 * Created by Roman Shylo on 05.12.2016.
 * whoose.daddy@gmail.com
 */
public class DialogUtils {

    public interface DialogOkClickCallback {
        void onOkClicked();
    }

    public interface DialogCancelClickCallback {
        void onCancelClicked();
    }

    public static void createSimple(Context context, String message) {
        createSimple(context, null, message, null, null);
    }

    public static void createSimple(Context context, @Nullable
            String title, String message, final DialogOkClickCallback okCallback, final DialogCancelClickCallback cancelCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title == null) {
            builder.setTitle(R.string.app_name);
        } else {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (okCallback != null) okCallback.onOkClicked();
            }
        });
        if (cancelCallback != null) {
            builder.setNegativeButton(R.string.dialog_cancel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                            cancelCallback.onCancelClicked();
                        }
                    });
        }
        builder.create().show();
    }

}