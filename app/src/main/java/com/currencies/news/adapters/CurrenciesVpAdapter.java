package com.currencies.news.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.currencies.news.views.currency.CurrencyFragment;
import com.currencies.news.views.currency.LoadData;
import com.currencies.news.views.currency_today.CurrencyTodayFragment;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class CurrenciesVpAdapter extends FragmentStatePagerAdapter {

    private List<LoadData> items;

    private SparseArray<WeakReference<Fragment>> fragments;

    public CurrenciesVpAdapter(FragmentManager fm, List<LoadData> items) {
        super(fm);
        this.items = items;
        fragments = new SparseArray<>();
    }

    @Override public Fragment getItem(int position) {
        Fragment fragment = getWeakFragment(position);
        if (fragment == null) {
            if (position == 0) {
                // Show today page for first item
                fragment = CurrencyTodayFragment.newInstance();
            } else {
                LoadData data = items.get(position);
                fragment = CurrencyFragment.newInstance(data);
            }
            fragments.put(position, new WeakReference<>(fragment));
        }
        return fragment;
    }

    @Override public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override public CharSequence getPageTitle(int position) {
        return items.get(position).getTitle();
    }

    public Fragment getWeakFragment(int position) {
        return fragments != null && position < fragments.size()
                ? fragments.get(position).get() : null;
    }
}
