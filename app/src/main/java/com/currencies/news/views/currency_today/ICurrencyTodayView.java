package com.currencies.news.views.currency_today;

import com.currencies.news.base_arch.presentation.IBaseView;
import com.currencies.news.views.IRefreshableView;

/**
 * Created by Roman Shylo on 02.06.2017.
 * whoose.daddy@gmail.com
 */

public interface ICurrencyTodayView extends IBaseView, IRefreshableView {

    void setRate(float rate);
}
