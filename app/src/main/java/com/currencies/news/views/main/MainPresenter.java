package com.currencies.news.views.main;

import com.currencies.news.R;
import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.views.currency.LoadData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class MainPresenter extends BasePresenter<IMainView> {

    private int[] dataValues;
    private String[] dataLabels;

    public MainPresenter(IMainView view) {
        super(view);
    }

    @Override public void init() {
        dataLabels = view.getDataLabels();
        dataValues = view.getDataValues();
        view.setDataItems(generateDataItems());
        view.showShortMessage(string(R.string.main_messageInfoRefreshCase));
    }

    ///////////////////////////////////////////////////////////////////////////
    // ACTIONS
    ///////////////////////////////////////////////////////////////////////////

    void actionUpdate() {
        view.updateSelectedPage();
    }

    private List<LoadData> generateDataItems() {
        if (dataValues.length != dataLabels.length) {
            throw new RuntimeException("Arrays must be the same size");
        }
        List<LoadData> items = new ArrayList<>(dataLabels.length);
        for (int i = 0; i < dataValues.length; i++) {
            items.add(new LoadData(dataValues[i], dataLabels[i]));
        }
        return items;
    }

}
