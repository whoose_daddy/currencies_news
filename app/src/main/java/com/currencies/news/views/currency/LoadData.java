package com.currencies.news.views.currency;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class LoadData {

    private int daysPeriod;
    private String title;

    public LoadData(int daysPeriod, String title) {
        this.daysPeriod = daysPeriod;
        this.title = title;
    }

    public int getDaysPeriod() {
        return daysPeriod;
    }

    public String getTitle() {
        return title;
    }
}
