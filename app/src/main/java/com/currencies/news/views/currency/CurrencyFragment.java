package com.currencies.news.views.currency;

import android.view.View;

import com.currencies.news.R;
import com.currencies.news.base_arch.fragment.BaseFragment;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class CurrencyFragment extends BaseFragment<CurrencyPresenter> implements ICurrencyView {

    // Bind views
    @BindView(R.id.currency_chart) LineChart mChart;
    @BindView(R.id.currency_progress) View progress;
    // Bind res
    @BindDimen(R.dimen.marginHalf) int chartOffset;
    @BindColor(R.color.chart_circleBg) int colorChartCircleBg;
    @BindColor(R.color.chart_valueText) int colorChartValueText;
    @BindColor(R.color.chart_lineData) int colorChartLineData;
    @BindColor(R.color.chart_axisColor) int colorChartAxisColor;
    @BindColor(R.color.chart_axisText) int colorChartAxisText;

    private LoadData data;

    public static CurrencyFragment newInstance(LoadData data) {
        CurrencyFragment fragment = new CurrencyFragment();
        fragment.setData(data);
        return fragment;
    }

    public void setData(LoadData data) {
        this.data = data;
    }

    @Override protected CurrencyPresenter createPresenter() {
        if (data == null) {
            throw new IllegalStateException("Data must be set before showing");
        }
        return new CurrencyPresenter(this, data);
    }

    @Override protected int getLayoutId() {
        return R.layout.fragment_currency;
    }

    @Override protected void findViews(View rootView) {
        super.findViews(rootView);
        setupChart();
    }

    @Override public void showProgress() {
        progress.setVisibility(View.VISIBLE);
        mChart.setVisibility(View.INVISIBLE);
    }

    @Override public void hideProgress() {
        progress.setVisibility(View.GONE);
        mChart.setVisibility(View.VISIBLE);
    }

    ///////////////////////////////////////////////////////////////////////////
    // SETUP
    ///////////////////////////////////////////////////////////////////////////

    private void setupChart() {
        mChart.setExtraRightOffset(chartOffset);
        mChart.setExtraBottomOffset(chartOffset * 2);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        //        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(300);

        XAxis x = mChart.getXAxis();
        x.setEnabled(true);
        x.setTextColor(colorChartAxisText);
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setDrawGridLines(true);
        x.setAxisLineColor(colorChartAxisColor);
        x.setLabelRotationAngle(-45);
        x.setGranularity(1f); // only intervals of 1 day

        YAxis y = mChart.getAxisLeft();
        y.setXOffset(chartOffset * 1.5f);
        y.setLabelCount(6, false);
        //        y.setGranularity(1f); // only intervals of 1 value
        y.setTextColor(colorChartAxisText);
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        y.setDrawGridLines(false);
        y.setAxisLineColor(colorChartAxisColor);

        mChart.getAxisRight().setEnabled(false);

        mChart.getLegend().setEnabled(false);

        // don't forget to refresh the drawing
        //        mChart.invalidate();
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEW IMPL.
    ///////////////////////////////////////////////////////////////////////////

    @Override public void setRates(float[] rates, String[] dates) {
        mChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(dates));
        setRatesData(rates);
        mChart.animateXY(0, 1500);
        mChart.invalidate();
    }

    @Override public void refresh() {
        presenter.actionRefresh();
    }

    ///////////////////////////////////////////////////////////////////////////
    // INNER
    ///////////////////////////////////////////////////////////////////////////

    private void setRatesData(float[] rates) {

        ArrayList<Entry> yVals = new ArrayList<>();

        for (int i = 0; i < rates.length; i++) {
            yVals.add(new Entry(i, rates[i]));
        }

        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(yVals, "");

            set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
            set1.setCubicIntensity(0.1f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(true);
            set1.setLineWidth(1.1f);
            set1.setCircleRadius(4f);
            set1.setCircleColor(colorChartCircleBg);
            set1.setColor(colorChartLineData);
            set1.setFillDrawable(getResources().getDrawable(R.drawable.gradient_chart_white));
            set1.setDrawHighlightIndicators(false);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return -10;
                }
            });

            // create a data object with the datasets
            LineData data = new LineData(set1);
            data.setValueTextSize(9f);
            data.setValueTextColor(colorChartValueText);
            data.setDrawValues(true);

            // set data
            mChart.setData(data);
        }
    }

}
