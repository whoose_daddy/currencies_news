package com.currencies.news.views.currency_today;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.currencies.news.R;
import com.currencies.news.base_arch.fragment.BaseFragment;

import butterknife.BindView;

/**
 * Created by Roman Shylo on 02.06.2017.
 * whoose.daddy@gmail.com
 */

public class CurrencyTodayFragment extends BaseFragment<CurrencyTodayPresenter>
        implements ICurrencyTodayView {

    @BindView(R.id.currencyToday_vgRateInfo) ViewGroup vgRateInfo;
    @BindView(R.id.currencyToday_tvRate) TextView tvRate;
    @BindView(R.id.currencyToday_progress) View progress;

    public static CurrencyTodayFragment newInstance() {
        return new CurrencyTodayFragment();
    }

    @Override protected CurrencyTodayPresenter createPresenter() {
        return new CurrencyTodayPresenter(this);
    }

    @Override protected int getLayoutId() {
        return R.layout.fragment_currency_today;
    }

    @Override public void showProgress() {
        vgRateInfo.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
    }

    @Override public void hideProgress() {
        vgRateInfo.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEW IMPL.
    ///////////////////////////////////////////////////////////////////////////

    @Override public void setRate(float rate) {
        tvRate.setText(String.valueOf(rate));
    }

    @Override public void refresh() {
        presenter.actionRefresh();
    }
}
