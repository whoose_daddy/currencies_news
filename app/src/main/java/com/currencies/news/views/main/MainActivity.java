package com.currencies.news.views.main;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.currencies.news.App;
import com.currencies.news.R;
import com.currencies.news.adapters.CurrenciesVpAdapter;
import com.currencies.news.base_arch.activity.BaseActivity;
import com.currencies.news.views.IRefreshableView;
import com.currencies.news.views.currency.LoadData;

import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainView {

    @BindView(R.id.main_tabLayout) TabLayout tabLayout;
    @BindView(R.id.main_viewPager) ViewPager viewPager;

    @BindArray(R.array.currencies_titles) String[] dataTitles;
    @BindArray(R.array.currencies_values) int[] dataValues;

    private CurrenciesVpAdapter adapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override protected MainPresenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override public void injectComponents() {
        App.getApp().inject(this);
    }

    @Override protected void findViews() {
        super.findViews();
        setupVP();
    }

    private void setupVP() {
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override public void onTabReselected(TabLayout.Tab tab) {
                presenter.actionUpdate();
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWS IMPL.
    ///////////////////////////////////////////////////////////////////////////


    @Override public int[] getDataValues() {
        return dataValues;
    }

    @Override public String[] getDataLabels() {
        return dataTitles;
    }

    @Override public void setDataItems(List<LoadData> items) {
        adapter = new CurrenciesVpAdapter(getSupportFragmentManager(), items);
        viewPager.setAdapter(adapter);
    }

    @Override public void updateSelectedPage() {
        Fragment current = adapter.getWeakFragment(viewPager.getCurrentItem());
        if (current != null && current instanceof IRefreshableView) {
            ((IRefreshableView) current).refresh();
        }
    }
}
