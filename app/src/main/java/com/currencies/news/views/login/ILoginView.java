package com.currencies.news.views.login;

import com.currencies.news.base_arch.presentation.IBaseView;

/**
 * Created by Roman Shylo on 13.04.2017.
 * whoose.daddy@gmail.com
 */

public interface ILoginView extends IBaseView {

    void setErrorEmail(boolean error);

    void setErrorPassword(boolean error);

    String getEmail();

    String getPassword();

    void showLoginError();

    void showSuccessScreen();

}