package com.currencies.news.views.currency_today;

import com.currencies.news.App;
import com.currencies.news.api.DefaultCallback;
import com.currencies.news.api.IRetrofitApi;
import com.currencies.news.api.entities.Currency;
import com.currencies.news.base_arch.presentation.BasePresenter;

import retrofit2.Response;

/**
 * Created by Roman Shylo on 02.06.2017.
 * whoose.daddy@gmail.com
 */

public class CurrencyTodayPresenter extends BasePresenter<ICurrencyTodayView> {

    private IRetrofitApi api;

    public CurrencyTodayPresenter(ICurrencyTodayView view) {
        super(view);
        api = App.getApp().api();
    }

    @Override public void init() {
        loadRate();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ACTIONS
    ///////////////////////////////////////////////////////////////////////////

    void actionRefresh() {
        loadRate();
    }

    ///////////////////////////////////////////////////////////////////////////
    // LOGIC
    ///////////////////////////////////////////////////////////////////////////

    private void loadRate() {
        view.showProgress();
        api.getLatest().enqueue(new DefaultCallback<Currency>(view) {
            @Override public void onResponse(Response<Currency> response) {
                view.hideProgress();
                view.setRate(response.body().getBaseRate());
            }
        });
    }
}
