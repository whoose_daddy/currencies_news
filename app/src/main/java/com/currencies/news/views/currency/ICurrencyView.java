package com.currencies.news.views.currency;

import com.currencies.news.base_arch.presentation.IBaseView;
import com.currencies.news.views.IRefreshableView;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public interface ICurrencyView extends IBaseView, IRefreshableView {

    void setRates(float[] rates, String[] dates);
}
