package com.currencies.news.views.currency;

import android.support.v4.util.Pair;

import com.currencies.news.App;
import com.currencies.news.api.IRetrofitApi;
import com.currencies.news.api.task.GetCurrenciesByDate;
import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.utils.AppTimeUtils;

import org.joda.time.DateTime;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class CurrencyPresenter extends BasePresenter<ICurrencyView> {

    private IRetrofitApi api;

    private LoadData data;

    private float[] rates;
    private String[] dates;

    public CurrencyPresenter(ICurrencyView view, LoadData data) {
        super(view);
        this.data = data;
        this.api = App.getApp().api();
    }

    @Override public void init() {
        cleanLoad();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ACTIONS
    ///////////////////////////////////////////////////////////////////////////

    void actionRefresh() {
        cleanLoad();
    }

    ///////////////////////////////////////////////////////////////////////////
    // LOGIC
    ///////////////////////////////////////////////////////////////////////////

    private void cleanLoad() {
        loadByPeriod(data.getDaysPeriod());
    }

    private void loadByPeriod(int days) {
        DateTime now = DateTime.now();
        loadRates(now.minusDays(days), now);
    }

    private void loadRates(DateTime from, DateTime to) {
        view.showProgress();
        //noinspection unchecked
        new GetCurrenciesByDate(api, new GetCurrenciesByDate.IGetCurrenciesCallback() {
            @Override
            public void onCurrenciesReceived(List<GetCurrenciesByDate.CurrencyItem> currencies) {
                view.hideProgress();
                Timber.i("onCurrenciesReceived -> currencies[%s]", currencies);
                onCurrenciesLoaded(currencies);
            }

        }).execute(new Pair<>(from, to));
    }

    private void onCurrenciesLoaded(List<GetCurrenciesByDate.CurrencyItem> list) {
        rates = new float[list.size()];
        dates = new String[list.size()];
        for (GetCurrenciesByDate.CurrencyItem c : list) {
            int index = list.indexOf(c);
            rates[index] = c.getCurrency() != null ? c.getCurrency().getBaseRate() : 0;
            dates[index] = AppTimeUtils.formatDateShort(c.getDateTime());
        }
        view.setRates(rates, dates);
    }
}
