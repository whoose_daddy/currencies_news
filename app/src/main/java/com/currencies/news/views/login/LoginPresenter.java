package com.currencies.news.views.login;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Patterns;

import com.currencies.news.BuildConfig;
import com.currencies.news.app.Const;
import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.providers.IAuthProvider;

/**
 * Created by Roman Shylo on 13.04.2017.
 * whoose.daddy@gmail.com
 */
@SuppressWarnings("WeakerAccess")
public class LoginPresenter extends BasePresenter<ILoginView> {

    private static final int MIN_PASS_LENGTH = 6;

    private IAuthProvider authProvider;

    public LoginPresenter(ILoginView view, IAuthProvider authProvider) {
        super(view);
        this.authProvider = authProvider;
    }

    @Override public void init() {
        // Show login hint with test credentials
        view.showShortMessage(generateLoginHint());
    }

    boolean isNeedSkipLogin() {
        return authProvider.isAuthorized();
    }

    ///////////////////////////////////////////////////////////////////////////
    // ACTIONS
    ///////////////////////////////////////////////////////////////////////////

    void actionEmailChanged(String newEmail) {
        validateEmail(newEmail);
    }

    void actionPassChanged(String newPass) {
        validatePass(newPass);
    }

    void actionLogin() {
        String email = view.getEmail();
        String pass = view.getPassword();
        if (validateData(email, pass)) {
            login(email, pass);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // INNER
    ///////////////////////////////////////////////////////////////////////////

    private String generateLoginHint() {
        return "Login: " + BuildConfig.MASTER_LOGIN + "\n" + "Pass: " + BuildConfig.MASTER_PASS;
    }

    private boolean validateData(String email, String pass) {
        return validateEmail(email) & validatePass(pass);
    }

    private boolean validateEmail(String email) {
        boolean hasError =
                TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches();
        view.setErrorEmail(hasError);
        return !hasError;
    }

    private boolean validatePass(String pass) {
        boolean hasError = TextUtils.isEmpty(pass) || pass.length() < MIN_PASS_LENGTH;
        view.setErrorPassword(hasError);
        return !hasError;
    }

    private void login(final String email, final String pass) {
        view.showProgress();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                view.hideProgress();
                if (BuildConfig.MASTER_LOGIN.contentEquals(email)
                        && BuildConfig.MASTER_PASS.contentEquals(pass)) {
                    onSuccessLogin(email);
                } else {
                    onFailedLogin();
                }
            }
        }, Const.FAKE_LOADING_TIME);
    }

    private void onFailedLogin() {
        view.setErrorEmail(true);
        view.setErrorPassword(true);
        view.showLoginError();
    }

    private void onSuccessLogin(String email) {
        authProvider.saveUser(email);
        view.showSuccessScreen();
    }

}