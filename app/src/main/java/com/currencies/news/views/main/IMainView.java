package com.currencies.news.views.main;

import com.currencies.news.base_arch.presentation.IBaseView;
import com.currencies.news.views.currency.LoadData;

import java.util.List;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public interface IMainView extends IBaseView {

    int[] getDataValues();

    String[] getDataLabels();

    void setDataItems(List<LoadData> items);

    void updateSelectedPage();

}
