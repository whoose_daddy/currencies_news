package com.currencies.news.views;

/**
 * Created by Roman Shylo on 02.06.2017.
 * whoose.daddy@gmail.com
 */

public interface IRefreshableView {

    void refresh();
}
