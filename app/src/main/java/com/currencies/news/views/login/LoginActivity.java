package com.currencies.news.views.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.currencies.news.App;
import com.currencies.news.views.main.MainActivity;
import com.currencies.news.R;
import com.currencies.news.base_arch.activity.BaseActivity;
import com.currencies.news.providers.IAuthProvider;
import com.currencies.news.utils.TextWatcherAdapter;
import com.currencies.news.widgets.AppToolbar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Roman Shylo on 13.04.2017.
 * whoose.daddy@gmail.com
 */

public class LoginActivity extends BaseActivity<LoginPresenter> implements ILoginView {

    @BindView(R.id.app_toolbar) AppToolbar toolbar;
    @BindView(R.id.login_etEmail) EditText etEmail;
    @BindView(R.id.login_etPassword) EditText etPassword;
    @BindView(R.id.login_viewErrorEmail) View viewErrorEmail;
    @BindView(R.id.login_viewErrorPass) View viewErrorPass;

    @Inject IAuthProvider authProvider;

    @Override protected LoginPresenter createPresenter() {
        return new LoginPresenter(this, authProvider);
    }

    @Override protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override public void injectComponents() {
        App.getApp().inject(this);
    }

    @Override protected boolean onPreCreateLayout() {
        boolean skip = presenter.isNeedSkipLogin();
        if (skip) {
            showSuccessScreen();
        }
        return skip;
    }

    @Override protected void findViews() {
        super.findViews();
        toolbar.setupWithActivity(this);
        toolbar.setTitle(R.string.titleScreen_login);
        setupInputs();
    }

    private void setupInputs() {
        etEmail.addTextChangedListener(createInputTextWatcher(etEmail));
        etPassword.addTextChangedListener(createInputTextWatcher(etPassword));
    }

    @OnClick(R.id.login_btnLogin)
    public void onClickLogin() {
        presenter.actionLogin();
    }

    private TextWatcher createInputTextWatcher(final EditText editText) {
        return new TextWatcherAdapter() {
            @Override public void afterTextChanged(Editable s) {
                switch (editText.getId()) {
                    case R.id.login_etEmail:
                        presenter.actionEmailChanged(s.toString());
                        break;
                    case R.id.login_etPassword:
                        presenter.actionPassChanged(s.toString());
                        break;
                }
            }
        };
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEW IMPL.
    ///////////////////////////////////////////////////////////////////////////

    @Override public void setErrorEmail(boolean error) {
        etEmail.setActivated(error);
        viewErrorEmail.setVisibility(error ? View.VISIBLE : View.INVISIBLE);
    }

    @Override public void setErrorPassword(boolean error) {
        etPassword.setActivated(error);
        viewErrorPass.setVisibility(error ? View.VISIBLE : View.INVISIBLE);
    }

    @Override public String getEmail() {
        return etEmail.getText().toString().trim();
    }

    @Override public String getPassword() {
        return etPassword.getText().toString().trim();
    }

    @Override public void showLoginError() {
        showShortMessage(getString(R.string.messageError_login));
    }

    @Override public void showSuccessScreen() {
        startActivity(MainActivity.createIntent(ctx()));
        finish();
    }
}
