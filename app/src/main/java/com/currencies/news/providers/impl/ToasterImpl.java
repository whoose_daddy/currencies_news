package com.currencies.news.providers.impl;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.currencies.news.providers.IToaster;

import java.util.Locale;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class ToasterImpl implements IToaster {

    private Context context;

    public ToasterImpl(Context context) {
        this.context = context;
    }

    @Override public void show(@StringRes int messageRes) {
        show(context.getString(messageRes));
    }

    @Override public void show(String message, Object... args) {
        String formatted = String.format(Locale.US, message, args);
        show(formatted);
    }

    @Override public void show(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
