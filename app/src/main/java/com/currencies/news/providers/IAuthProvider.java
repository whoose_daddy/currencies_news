package com.currencies.news.providers;

/**
 * Created by Roman Shylo on 12.04.2017.
 * whoose.daddy@gmail.com
 */

public interface IAuthProvider {

    boolean isAuthorized();

    void logout();

    void saveUser(String email);

}