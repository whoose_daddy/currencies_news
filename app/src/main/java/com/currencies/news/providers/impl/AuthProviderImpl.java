package com.currencies.news.providers.impl;

import android.text.TextUtils;

import com.currencies.news.providers.IAuthProvider;
import com.currencies.news.providers.IPreferences;

/**
 * Created by Roman Shylo on 12.04.2017.
 * whoose.daddy@gmail.com
 */

public class AuthProviderImpl implements IAuthProvider {

    private IPreferences preferences;

    public AuthProviderImpl(IPreferences preferences) {
        this.preferences = preferences;
    }

    @Override public boolean isAuthorized() {
        return !TextUtils.isEmpty(preferences.getUserEmail());
    }

    @Override public void logout() {
        preferences.clearAll();
    }

    @Override public void saveUser(String email) {
        preferences.storeUserEmail(email);
    }


}