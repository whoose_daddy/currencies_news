package com.currencies.news.providers;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public interface IPreferences {

    interface KEYS {
        // define keys
        String USER_EMAIL = "user_email";
    }

    void clearAll();

    void storeUserEmail(String email);

    String getUserEmail();
}
