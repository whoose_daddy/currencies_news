package com.currencies.news.providers;

import android.support.annotation.StringRes;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public interface IToaster {

    void show(@StringRes int messageRes);

    void show(String message, Object... args);

    void show(String message);
}
