package com.currencies.news.providers.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.currencies.news.providers.IPreferences;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */

public class PreferencesImpl implements IPreferences {

    private SharedPreferences preferences;

    public PreferencesImpl(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override public void clearAll() {
        preferences.edit().clear().apply();
    }

    @Override public void storeUserEmail(String email) {
        preferences.edit().putString(KEYS.USER_EMAIL, email).apply();
    }

    @Override public String getUserEmail() {
        return preferences.getString(KEYS.USER_EMAIL, null);
    }
}
