package com.currencies.news.di;

import com.currencies.news.api.IRetrofitApi;
import com.currencies.news.api.RetrofitService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    IRetrofitApi provideApi(Gson gson) {
        return new RetrofitService(gson).getRetrofitApi();
    }

}
