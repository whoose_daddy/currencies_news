package com.currencies.news.di;

import android.content.Context;

import com.currencies.news.views.main.MainActivity;
import com.currencies.news.api.IRetrofitApi;
import com.currencies.news.providers.IPreferences;
import com.currencies.news.providers.IToaster;
import com.currencies.news.views.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
@Singleton
@Component(modules = {AppModule.class, DataModule.class, NetworkModule.class})
public interface AppComponent {

    Context ctx();

    IToaster toaster();

    IPreferences preferences();

    IRetrofitApi api();

    // Injects
    void inject(MainActivity target);

    void inject(LoginActivity target);

}