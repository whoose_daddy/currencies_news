package com.currencies.news.di;

import android.content.Context;

import com.currencies.news.providers.IAuthProvider;
import com.currencies.news.providers.IPreferences;
import com.currencies.news.providers.impl.AuthProviderImpl;
import com.currencies.news.providers.impl.PreferencesImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    IPreferences providePreferences(Context context) {
        return new PreferencesImpl(context);
    }

    @Provides
    @Singleton
    IAuthProvider provideAuthProvider(IPreferences preferences) {
        return new AuthProviderImpl(preferences);
    }

}