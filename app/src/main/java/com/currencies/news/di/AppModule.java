package com.currencies.news.di;

import android.content.Context;

import com.currencies.news.providers.IToaster;
import com.currencies.news.providers.impl.ToasterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roman Shylo on 01.06.2017.
 * whoose.daddy@gmail.com
 */
@Module
public class AppModule {

    private Context appContext;

    public AppModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return appContext;
    }

    @Provides
    @Singleton
    IToaster provideToaster(Context context) {
        return new ToasterImpl(context);
    }

}