package com.currencies.news.base_arch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.currencies.news.App;
import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.base_arch.presentation.IBaseView;
import com.currencies.news.utils.DialogUtils;

import butterknife.ButterKnife;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements IBaseView {

    protected T presenter;
    protected View root;

    protected abstract T createPresenter();

    protected abstract int getLayoutId();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponents();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null || presenter == null || isUpdatable()) {
            root = inflater.inflate(getLayoutId(), container, false);
            presenter = createPresenter();
            findViews(root);
            setupActionBar(getSupportActionBar());
            presenter.init();
        }
        return root;
    }

    public void injectComponents() {
        // Nothing
    }

    protected void findViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    protected void setupActionBar(ActionBar actionBar) {
        if (actionBar == null) {
            return;
        }
    }

    protected ActionBar getSupportActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public void showProgress() {
        if (getActivity() != null) {
            ((IBaseView) getActivity()).showProgress();
        }
    }

    @Override
    public void hideProgress() {
        if (getActivity() != null) {
            ((IBaseView) getActivity()).hideProgress();
        }
    }

    @Override
    public void showShortMessage(String message) {
        App.getApp().toaster().show(message);
    }

    @Override
    public void showDialog(String message) {
        DialogUtils.createSimple(ctx(), message);
    }

    protected void closeKeyboard() {
        if (getView() == null) {
            return;
        }
        View view = getView().findFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Checks whether we need to re-draw and update the view
     *
     * @return
     */
    protected boolean isUpdatable() {
        return false;
    }

    @Override
    public Context ctx() {
        return getContext();
    }

}
