package com.currencies.news.base_arch.presentation;

import android.content.Context;

public interface IBaseView {

    void showProgress();

    void hideProgress();

    void showShortMessage(String message);

    void showDialog(String message);

    Context ctx();

}