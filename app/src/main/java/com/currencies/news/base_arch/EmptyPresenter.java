package com.currencies.news.base_arch;


import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.base_arch.presentation.IBaseView;

/**
 * Created by Roman Shylo on 11.04.2017.
 * whoose.daddy@gmail.com
 */

public class EmptyPresenter extends BasePresenter<IBaseView> {

    public EmptyPresenter(IBaseView view) {
        super(view);
    }

    @Override public void init() {

    }
}
