package com.currencies.news.base_arch.presentation;

import android.support.annotation.StringRes;

public abstract class BasePresenter<T extends IBaseView> {

    protected T view;

    public BasePresenter(T view) {
        this.view = view;
    }

    public abstract void init();

    public void onViewReady() {
        // Nothing
    }

    public void onViewDestroyed() {
        // Nothing
    }

    public void onPaused() {
        // Nothing
    }

    public void onResumed() {
        // Nothing
    }

    public String string(@StringRes int res) {
        return view.ctx().getString(res);
    }

}
