package com.currencies.news.base_arch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.currencies.news.R;
import com.currencies.news.base_arch.presentation.BasePresenter;
import com.currencies.news.base_arch.presentation.IBaseView;
import com.currencies.news.providers.IToaster;
import com.currencies.news.utils.DialogUtils;
import com.currencies.news.utils.KeyboardFn;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity
        implements IBaseView {

    @Inject protected IToaster toaster;

    protected ProgressDialog progressDialog;

    protected T presenter;

    protected abstract T createPresenter();

    protected abstract int getLayoutId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponents();
        presenter = createPresenter();
        if (onPreCreateLayout()) {
            return;
        }
        setContentView(getLayoutId());
        findViews();
        presenter.init();
        presenter.onViewReady();
    }

    public abstract void injectComponents();

    @Override protected void onPause() {
        presenter.onPaused();
        super.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        presenter.onResumed();
    }

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.onViewDestroyed();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called before activity's #OnCreate(Bundle state).setContentView(int id)
     *
     * @return TRUE if no needs to create activity and set Content View to it;
     * FALSE - create activity as default
     */
    protected boolean onPreCreateLayout() {
        return false;
    }

    protected void findViews() {
        ButterKnife.bind(this);
    }

    @Override public void showProgress() {
        runOnUiThread(new Runnable() {
            @Override public void run() {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(ctx());
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage(getString(R.string.messageDialog_loading));
                    progressDialog.show();
                }
            }
        });
    }

    @Override
    public void hideProgress() {
        runOnUiThread(new Runnable() {
            @Override public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });
    }

    @Override
    public void showShortMessage(String message) {
        toaster.show(message);
    }

    @Override
    public void showDialog(String message) {
        DialogUtils.createSimple(ctx(), message);
    }

    @Override
    public Context ctx() {
        return this;
    }

    protected void closeKeyboard() {
        KeyboardFn.closeKeyboard(this);
    }

}